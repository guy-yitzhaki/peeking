import ConfigParser as configparser


def read_config(filename):
	trigger_pins = []
	echo_pins = []
	trigger_distances = []
	stop_trigger_distances = []
	sounds = []
	relay_pins = []
	ignore_distances = []
	max_distances = []
	sample_sizes = []
	trigger_sample_count = []
	stop_trigger_sample_count = []

	config = configparser.ConfigParser()
	config.read(filename)

	for i in range(1, 9):
		key = "SENSOR%d" % i
		if config.has_section(key):
			trigger_pins.append(config.getint(key, 'trigger_pin'))
			echo_pins.append(config.getint(key, 'echo_pin'))
			relay_pins.append(config.getint(key, 'relay_pin'))
			trigger_distances.append(config.getint(key, 'trigger_distance'))
			stop_trigger_distances.append(config.getint(key, 'stop_trigger_distance'))
			sounds.append(config.get(key, 'sound'))
			ignore_distances.append(config.getint(key, 'ignore_distance'))
			max_distances.append(config.getint(key, 'max_distance'))
			sample_sizes.append(config.getint(key, 'sample_size'))
			trigger_sample_count.append(config.getint(key, 'trigger_sample_count'))
			stop_trigger_sample_count.append(config.getint(key, 'stop_trigger_sample_count'))

	return {
		'trigger_pins': trigger_pins,
		'echo_pins': echo_pins,
		'relay_pins': relay_pins,
		'trigger_distances': trigger_distances,
		'max_distances': max_distances,
		'ignore_distances': ignore_distances,
		'sounds': sounds,
		'sample_sizes': sample_sizes,
		'stop_trigger_distances': stop_trigger_distances,
		'trigger_sample_count': trigger_sample_count,
		'stop_trigger_sample_count': stop_trigger_sample_count,
		'port': config.get('ARDUINO', 'port'),
		'baud': config.getint('ARDUINO', 'baud'),
		'loop_interval': config.getfloat('SYSTEM', 'loop_interval'),
		'main_loop_sleep': config.getfloat('SYSTEM', 'main_loop_sleep'),
		'ignore_range': config.getint('SYSTEM', 'ignore_range'),
	}
