from __future__ import print_function
import time
import ConfigParser as configparser
from conf import read_config

env_config = configparser.ConfigParser()
env_config.read('env.config')
relay_type = env_config.get('ENV', 'relay_type')
simulate_arduino = env_config.getboolean('ENV', 'simulate_arduino')
simulate_sensors = env_config.getboolean('ENV', 'simulate_sensors')
max_sensors = env_config.getint('ENV', 'max_sensors')
use_sound = env_config.getboolean('ENV', 'use_sound')
debug = env_config.getboolean('ENV', 'debug')
debug_sensor = env_config.getint('ENV', 'debug_sensor')

if use_sound:
	import pygame as pg

if not simulate_sensors:
	from ultrasonic_hcsr04 import *
else:
	from simulate_hcsr04 import *

if relay_type == 'simulate':
	from simulate_relay import *
elif relay_type == 'arduino':
	from arduino_relay import *
elif relay_type == 'gpio':
	from gpio_relay import *


class Peeking:
	def __init__(self):
		config = read_config('peeking.config')
		self.relay = Relay()
		self.trigger_pins = config['trigger_pins']
		self.echo_pins = config['echo_pins']
		self.trigger_distances = config['trigger_distances']
		self.ignore_distances = config['ignore_distances']
		self.max_distances = config['max_distances']
		self.stop_trigger_distances = config['stop_trigger_distances']
		self.sample_sizes = config['sample_sizes']
		self.stop_trigger_distances = config['stop_trigger_distances']
		self.trigger_sample_count = config['trigger_sample_count']
		self.stop_trigger_sample_count = config['stop_trigger_sample_count']
		self.sounds = []
		self.channels = []
		self.samples = []
		sound_files = config['sounds']
		if use_sound:
			pg.mixer.init(channels=len(sound_files))
			sounds = [pg.mixer.Sound(name) for name in sound_files]
			for i in range(len(sounds)):
				self.channels.append(sounds[i].play(-1))
			for i in range(len(self.channels)):
				self.channels[i].pause()
		self.hcsr04 = HCSR04(self.trigger_pins, self.echo_pins)
		self.playing = []
		for i in range(len(self.trigger_pins)):
			self.playing.append(False)
			self.samples.append([self.max_distances[i] for x in range(self.sample_sizes[i])])
		print(self.samples)
		self.loop_interval = config['loop_interval']
		self.main_loop_sleep = config['main_loop_sleep']
		time.sleep(2)
		
	def loop(self):
		num_sensors = min(max_sensors, len(self.trigger_pins))
		while True:
			for index in range(num_sensors):
				cm = self.hcsr04.distance(self.trigger_pins[index], self.echo_pins[index])
				if cm > self.max_distances[index]:
					#print('%d: over max distance %d, trimming' % (index + 1, cm))
					cm = self.max_distances[index]
				if cm >= self.ignore_distances[index]:
					self.samples[index].append(cm)
					self.samples[index] = self.samples[index][1:]
					trigger_distance = self.trigger_distances[index]
					stop_trigger_distance = self.stop_trigger_distances[index]
					if not self.playing[index]:
						trigger_samples = [x for x in self.samples[index] if x <= trigger_distance]
						if len(trigger_samples) >= self.trigger_sample_count[index]:
							print("sensor %d triggered %d < %d" % (index + 1, cm, trigger_distance))
							self.play(index)
					else:
						stop_trigger_samples = [x for x in self.samples[index] if x >= stop_trigger_distance]
						if len(stop_trigger_samples) >= self.stop_trigger_sample_count[index]:
							print("sensor %d stopped %d > %d" % (index + 1, cm, trigger_distance))
							self.stop(index)

					if debug or index+1==debug_sensor:
						print("%d: %dcm  %s t=%d\t" % (index + 1, cm, str(self.samples[index]), trigger_distance))

				time.sleep(self.loop_interval)
			if debug:
				print('')
			time.sleep(self.main_loop_sleep)

	def play(self, index):
		print("playing sensor %d" % (index + 1))
		self.playing[index] = True
		if use_sound:
			self.channels[index].unpause()
		self.relay.on(index)

	def stop(self, index):
		print("stopping sensor %d" % (index + 1))
		self.playing[index] = False
		if use_sound:
			self.channels[index].pause()
		self.relay.off(index)

	def teardown(self):
		self.relay.teardown()
		self.hcsr04.teardown()


if __name__ == '__main__':
	peeking = Peeking()
	try:
		peeking.loop()
	except KeyboardInterrupt:
		print("stopped by user")
		peeking.teardown()
