import RPi.GPIO as GPIO
import time


class HCSR04:
	def __init__(self, trigger_pins, echo_pins):
		GPIO.setmode(GPIO.BCM)
		for pin in echo_pins:
			GPIO.setup(pin, GPIO.IN)
		for pin in trigger_pins:
			GPIO.setup(pin, GPIO.OUT)
			GPIO.output(pin, False)
		time.sleep(2)

	def teardown(self):
		GPIO.cleanup()

	def distance(self, trigger_pin, echo_pin):
		# set Trigger to HIGH
		GPIO.output(trigger_pin, True)
		# set Trigger after 0.01ms to LOW
		time.sleep(0.00001)
		GPIO.output(trigger_pin, False)

		start_time = time.time()
		stop_time = time.time()

		# save StartTime
		while GPIO.input(echo_pin) == 0:
			start_time = time.time()

		# save time of arrival
		while GPIO.input(echo_pin) == 1:
			stop_time = time.time()

		# time difference between start and arrival
		time_elapsed = stop_time - start_time
		# multiply with the sonic speed (34300 cm/s)
		# and divide by 2, because there and back
		distance_cm = (time_elapsed * 34300) / 2
		return distance_cm
