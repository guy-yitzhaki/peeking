import RPi.GPIO as GPIO
from conf import read_config


class Relay:
	def __init__(self):
		print('init relay')
		GPIO.setmode(GPIO.BCM)
		config = read_config('peeking.config')
		self.pins = config['relay_pins'] #[2, 3, 17, 4]
		print(self.pins)
		for i in self.pins:
			GPIO.setup(i, GPIO.OUT)
			GPIO.output(i, GPIO.LOW)

	def on(self, idx):
		print('relay on %d' % (idx + 1))
		GPIO.output(self.pins[idx], GPIO.HIGH)

	def off(self, idx):
		print('relay off %d' % (idx + 1))
		GPIO.output(self.pins[idx], GPIO.LOW)

	def teardown(self):
		for i in self.pins:
			GPIO.output(i, GPIO.LOW)
		GPIO.cleanup()
