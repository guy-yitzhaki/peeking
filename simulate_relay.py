
class Relay:
	def __init__(self):
		print('init relay')

	def on(self, idx):
		print('relay on %d' % (idx+1))
    	
	def off(self, idx):
    		print('relay off %d' % (idx+1))

	def teardown(self):
		print('tearing down relay')
    	
