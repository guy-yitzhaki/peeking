from __future__ import print_function
import ConfigParser as configparser
import time
from conf import read_config


env_config = configparser.ConfigParser()
env_config.read('env.config')
simulate_sensors = env_config.getboolean('ENV', 'simulate_sensors')
max_sensors = env_config.getint('ENV', 'max_sensors')

if not simulate_sensors:
	from ultrasonic_hcsr04 import *
else:
	from simulate_hcsr04 import *


if __name__ == '__main__':
    config = read_config('peeking.config')
    trigger_pins = config['trigger_pins']
    echo_pins = config['echo_pins']

    hcsr04 = HCSR04(trigger_pins, echo_pins)
    num_sensors = min(max_sensors, len(trigger_pins))
    try:
        for index in range(num_sensors):
            print("t: %d e: %d\t\t" % (trigger_pins[index], echo_pins[index]), end='')
            print("")
        while True:
            for index in range(num_sensors):
		time.sleep(0.5)
		dist = hcsr04.distance(trigger_pins[index], echo_pins[index])
                print("(%d)%03dcm\t\t" % ((index+1), dist), end='')
            print("")
            time.sleep(0.5)
            
    except KeyboardInterrupt:
        print("stopped by user")
        hcsr04.teardown()
