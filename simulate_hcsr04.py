import random

constant_data = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
trigger_data = [100, 100, 100, 100, 50, 50, 50, 50, 100, 100, 100, 100, 100, 100, 100, 100, 100]
noisy_no_trigger_data = [300, 260, 120, 50, 70, 70, 100, 1000, 100, 200, 50, 300, 100, 100, 100, 100, 100]
noisy_trigger_data = [180, 260, 120, 50, 70, 70, 50, 40, 300, 30, 30, 300, 100, 300, 200, 50, 100]


random_data = False


class HCSR04:
	def __init__(self, trigger_pins, echo_pins):
		print("setting up")
		self.data = {}
		self.idxs = {}

		for i in trigger_pins:
			self.data[i] = noisy_no_trigger_data
			self.idxs[i] = 0


	def distance(self, trigger_pin, echo_pin):
		if random_data:
			return random.randint(1, 100)
		sample = self.data[trigger_pin][self.idxs[trigger_pin]]
		self.idxs[trigger_pin] += 1
		if self.idxs[trigger_pin] >= len(self.data[trigger_pin]):
			self.idxs[trigger_pin] = 0
		return sample

	def teardown(self):
		print('tearing down hcsr04')