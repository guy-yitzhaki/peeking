// Vin to Relay VCC
// GND to Relay GND
// digital 6 to Relay 1
// digital 7 to Relay 2
// digital 8 to Relay 3
// digital 9 to Relay 4
// 220v brown conected
// 220v blue to middle 4 and left 4 (to bulb amd socket)

// serial 1-4: turn on relay 1-4
// serial 5-8: turn off relay 1-4

int RELAY_PINS[] = {6, 7, 8, 9};

void setup() {
    Serial.begin(9600);
    for (int i = 0; i < 4; i++) {
      pinMode(RELAY_PINS[i], OUTPUT);
      lightOff(i);
  }
}

void loop() {
  if (Serial.available() > 0) {
    // read the incoming byte:
    int incomingByte = Serial.read();
    switch (incomingByte) {
      case '1':
        lightOn(0);
        break;
      case '2':
        lightOn(1);
        break;
      case '3':
        lightOn(2);
        break;
      case '4':
        lightOn(3);
        break;
      case '5':
        lightOff(0);
        break;
      case '6':
        lightOff(1);
        break;      
      case '7':
        lightOff(2);
        break;      
      case '8':
        lightOff(3);
        break;
      default:
        Serial.println(incomingByte);
        Serial.println("unknown command");
        break;  
    }
  }
}

void lightOn(int idx) {
  Serial.print("on ");
  Serial.println(idx);
  digitalWrite(RELAY_PINS[idx], LOW);
}

void lightOff(int idx) {
  Serial.print("off ");
  Serial.println(idx);
  digitalWrite(RELAY_PINS[idx], HIGH);
}

