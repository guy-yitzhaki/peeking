from cmd import Cmd
import pygame as pg

sound_files = ['../sounds/audio1.wav', '../sounds/audio2.wav', '../sounds/audio3.wav', '../sounds/audio4.wav']
pg.mixer.init(channels=len(sound_files))
sounds = [pg.mixer.Sound(name) for name in sound_files]
channels = []
for i in range(len(sounds)):
	channels.append(sounds[i].play(-1))
for i in range(len(channels)):
	channels[i].pause()

class MusicPrompt(Cmd):
	def do_play(self, args):
		if len(args) == 0:
			num = 0
		else:
			num = int(args.split(' ')[0])
		if num < 1 or num > len(sounds):
			print('bad index')
		else: 
			idx = num - 1
			print('playing %s' % sound_files[idx])
			channels[idx].unpause()

	def do_pause(self, args):
		if len(args) == 0:
			num = 0
		else:
			num = int(args.split(' ')[0])
		if num < 1 or num > len(sounds):
			print('bad index')
		else: 
			idx = num - 1
			print('stopping %s' % sound_files[idx])
			channels[idx].pause()

	def do_quit(self, args):
		"""Quits the program."""
		print('quitting')
		raise SystemExit

if __name__ == '__main__':
	prompt = MusicPrompt()
	prompt.prompt = '> '
	prompt.cmdloop('starting...')