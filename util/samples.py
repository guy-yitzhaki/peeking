trigger = 60
stop_trigger = 90
sample_size = 3
trigger_sample_count = 2
stop_trigger_sample_count = 2

constant_data = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
trigger_data = [100, 100, 100, 100, 50, 50, 50, 50, 100, 100, 100, 100, 100, 100, 100, 100, 100]
noisy_no_trigger_data = [300, 260, 120, 50, 70, 70, 100, 1000, 100, 200, 50, 300, 100, 100, 100, 100, 100]
noisy_trigger_data = [300, 260, 120, 50, 70, 70, 50, 40, 300, 30, 30, 300, 100, 100, 100, 100, 100]


def should_trigger(samples):
	return True

def should_stop_trigger(samples):
	return True

def test_data(data, print_samples):
	samples = []
	triggered = False
	for i in data:
		samples.append(i)
		if len(samples) > sample_size:
			samples = samples[1:]
		if len(samples) == sample_size:
			if print_samples:
				print(samples)
			if not triggered:
				trigger_samples = [x for x in samples if x < trigger]
				if len(trigger_samples) >= trigger_sample_count:
					print('trigger!')
					triggered = True
			else:
				stop_trigger_samples = [x for x in samples if x > stop_trigger]
				if len(stop_trigger_samples) >= stop_trigger_sample_count:
					print('stop trigger!')
					triggered = False


print('constant:')
test_data(constant_data, False)
print('trigger:')
test_data(trigger_data, False)
print('noisy no trigger:')
test_data(noisy_no_trigger_data, True)
print('noisy trigger:')
test_data(noisy_trigger_data, True)



